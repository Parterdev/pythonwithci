# <a href="https://nodejs.org/es/" target="_blank"> <img src="https://assets.stickpng.com/images/5847f997cef1014c0b5e48c1.png" alt="gitlab" width="40" height="40"/> </a> GitLab Pipeline CI/CD App

Este proyecto ha sido construido en Python y será utilizado como base para las distintas fases de prueba en GitLab CI/CD.

### Como correr el proyecto

##### Requerimientos

- Python (> v3.6)

##### Crear entorno virtual y activarlo

```bash
pip install virtualenv
virtualenv venv
source venv/bin/activate
```

##### Instalar dependencias

```bash
pip install -r requirements.txt
```

### Enlaces relevantes

- [Google Cloud Console](https://console.cloud.google.com/)
- [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- [Git for Windows](https://gitforwindows.org/)
- [Install GitLab Runner](https://docs.gitlab.com/runner/install/)
- [Heroku](https://www.heroku.com/)
- [Dpl Tool](https://github.com/travis-ci/dpl)
- [Free for devs](https://free-for.dev/)